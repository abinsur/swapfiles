<div align="left">

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/TUX_G2.svg/800px-TUX_G2.svg.png" width="150">

</div>

# swapfiles and zram's

### fallocate

>### you're using [*BTRFS*](https://btrfs.readthedocs.io/en/latest/btrfs-man5.html#swapfile-support)?
>
>`# truncate -s 0 /swapfile`   
> set or adjust the file size by 0 bytes   
>
>`# chattr +C /swapfile`   
> for btrfs, the 'C' flag should be set on new or empty files    

`# fallocate -l 3gb /swapfile`   

`# chmod 600 /swapfile`   

`# mkswap /swapfile`   

`# swapon /swapfile`   

~~~
cat >> /etc/fstab << "EOF"  
# swapfile  
/swapfile   none    swap    default 0 0   
EOF  
~~~

---

### dd

>### you're using [*BTRFS*](https://btrfs.readthedocs.io/en/latest/btrfs-man5.html#swapfile-support)?
>`# truncate -s 0 /swapfile`   
> set or adjust the file size by 0 bytes   
>
>`# chattr +C /swapfile`   
> for btrfs, the 'C' flag should be set on new or empty files    

`# dd if=/dev/zero of=/swapfile bs=1M count=3072 status=progress`   

`# chmod 0600 /swapfile`   

`# mkswap /swapfile`   

`# swapon /swapfile`   

~~~
cat >> /etc/fstab << "EOF"  
# swapfile  
/swapfile   none    swap    default 0 0   
EOF
~~~  

---

### zramd

`$ git clone https://aur.archlinux.org/yay`  

`$ cd yay`   

`$ makepkg -si PKGBUILD`   

`$ cd .. && rm -rf yay`   

`$ yay -S zramd`  

>`# vim /etc/default/zramd`   
> configuration file

`# systemctl enable --now zramd.service`  

---

### systemd-zram

`$ yay -S systemd-zram`  
`# systemctl enable --now systemd-zram.service`  
[systemd-zram](https://github.com/mdomlop/systemd-zram)   

---

### zram-generator   

#### first
~~~
cat > /etc/systemd/zram-generator.conf << "EOF"
[zram0]
zram-size=ram / 2
compression-algorithm=zstd

[zram1]
mount-point=/var/compressed
compression-algorithm=zstd

[Service]
ExecStartPost=/bin/sh -c 'd=$(mktemp -d); mount "$1" "$d"; chmod 1777 "$d"; umount "$d"; rmdir "$d"' _ /dev/%i
EOF
~~~

#### second
`# pacman -S zram-generator`   

#### font
[01-manual-archlinux](https://man.archlinux.org/man/zram-generator.conf.5)   
[02-manual-archlinux](https://man.archlinux.org/man/zram-generator.8)   
[03-github-zram-generator](https://github.com/systemd/zram-generator)    

---

### swappiness   
>`# sysctl -a | grep -i swappiness`   
>`# more /proc/sys/vm/swappiness`   
> verify swapppiness   

~~~
cat > /etc/sysctl.d/swappiness.conf << "EOF"
vm.swappiness=100
EOF
~~~
> persist swappiness   

> `# sysctl -p /etc/sysctl.d/swappiness.conf`   
> load values  

|swapppiness|swap   |
| :-------: | :---: |
| 60        |40% RAM|
| 40        |60% RAM|
| 20        |80% RAM|
| 10        |90% RAM|
| 1         |99% RAM|

---

<div align="center">

#### [Tux (mascot), which is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License but not under the GFDL.](https://en.wikipedia.org/wiki/Tux_(mascot))

</div>